#ifndef LEDCUBE_cpp
#define LEDCUBE_cpp

#include "TimerOne.hpp"
#include "LedCube.h"

LedCube Cube;

void LedCube::begin(byte dimension, unsigned long refreshInterval, byte pinData, byte pinLatch, byte pinClock, byte layerPinOutType, ...)
{
	// Initialize data storage structure
	this->ledData.begin(dimension);

	// Load and set up shift register pins
	this->pinData = pinData;
	this->pinLatch = pinLatch;
	this->pinClock = pinClock;

	pinMode(pinData, OUTPUT);
	pinMode(pinLatch, OUTPUT);
	pinMode(pinClock, OUTPUT);
	
	va_list args;
	va_start(args, layerPinOutType);
	
	this->layerPinOutType = layerPinOutType;
	switch(layerPinOutType)
	{
		case LAYER_DEDICATED_PINS:
			// In the case of LAYER_DEDICATED_PINS, the individual pin indexes are expected as variadic parameters
			this->layerPins = new byte[dimension];
			
			for(int i = 0; i < dimension; i++)
			{
				this->layerPins[i] = (byte)va_arg(args, int);
				pinMode(this->layerPins[i], OUTPUT);
				digitalWrite(this->layerPins[i], LOW);
			}
			break;
		
		case DEDICATED_SHIFT_REGISTER:
			// In the case of DEDICATED_SHIFT_REGISTER, the data, latch and clock pin indexes are expected as variadic parameters
			this->layerPins = new byte[3];
			
			// LAYER_PIN_DATA = 0
			// LAYER_PIN_LATCH = 1
			// LAYER_PIN_CLOCK = 2
			for(int i = 0; i < 3; i++)
			{
				this->layerPins[i] = (byte)va_arg(args, int);
				pinMode(this->layerPins[i], OUTPUT);
			}
			break;
	}
	va_end(args);
	
	// Configure timer and handling
	this->refreshInterval = floor(refreshInterval / (float)dimension);
	
	Timer1.initialize(this->refreshInterval);
	Timer1.attachInterrupt(&LedCube::staticTimerCallback);
}

void LedCube::clear()
{
	this->ledData.clear();
}

void LedCube::clearLayer(byte layerIndex)
{
	this->ledData.clearLayer(layerIndex);
}

void LedCube::write(unsigned int index, bool state)
{
	this->ledData.setBit(index, state);
}

void LedCube::write(byte x, byte y, byte z, bool state)
{
	this->ledData.setBit(x + this->getDimension() * y + this->getLayerSize() * z, state);
}

void LedCube::writeBlock(byte layerIndex, byte blockIndex, byte value)
{
	this->ledData.setBlock(layerIndex, blockIndex, value);
}

void LedCube::writeLayerValue(byte layerIndex, unsigned long value)
{
	for(byte i = 0; i < 4 && i < this->getBlocksPerLayer(); i++)
	{
		this->ledData.setBlock(layerIndex, i, (value >> (i * 8)) & 0xFF);
	}
}

void LedCube::setBitOrder(byte order)
{
	this->bitOrder = order;
}

unsigned int LedCube::getSize()
{
	return this->ledData.getSize();
}

byte LedCube::getDimension()
{
	return this->ledData.getDimension();
}

byte LedCube::getLayerSize()
{
	return this->ledData.getLayerSize();
}

byte LedCube::getBlocksPerLayer()
{
	return this->ledData.getBlocksPerLayer();
}

void LedCube::timerCallback()
{
	byte layerCounter;
	layerCounter = (layerCounter + 1) % this->getDimension();

	// Render the next layer
	this->renderLayer();

	// After all layers were drawn (and the actual refresh interval has passed),
	// call the chained routine, if any.
	if(this->chainedRoutine != NULL && layerCounter == 0)
	{
		this->chainedRoutine();
	}
}

void LedCube::setChainedRoutine(void (*chainedRoutine)())
{
	this->chainedRoutine = chainedRoutine;
}

void LedCube::staticTimerCallback()
{
	Cube.timerCallback();
}

void LedCube::renderLayer()
{
	byte lastLayer = this->currentLayer;
	this->currentLayer = (this->currentLayer + 1) % this->getDimension();
 
	// Prepare for write out
	digitalWrite(this->pinLatch, LOW);

	// Serially send data to shift register
	byte* layerBlocks = this->ledData.getLayer(this->currentLayer);
	for(byte i = 0; i < this->getBlocksPerLayer(); i++)
	{
		shiftOut(this->pinData, this->pinClock, this->bitOrder, layerBlocks[i]);
	}

	// Turn off previous layer
	this->disableLayer(lastLayer);
  
	// Write out
	digitalWrite(this->pinLatch, HIGH);
  
	// Enable current layer
	this->enableLayer(this->currentLayer);
}

void LedCube::enableLayer(byte layerIndex)
{
	if(layerIndex >= this->getDimension())
		return;
		
	switch(this->layerPinOutType)
	{
		case LAYER_DEDICATED_PINS:
			digitalWrite(this->layerPins[layerIndex], HIGH);
			break;
		
		case DEDICATED_SHIFT_REGISTER:
			// TODO: Implement this
			digitalWrite(this->layerPins[layerIndex], HIGH);
			break;
	}	
}

void LedCube::disableLayer(byte layerIndex)
{
	if(layerIndex >= this->getDimension())
		return;
	
	switch(this->layerPinOutType)
	{
		case LAYER_DEDICATED_PINS:
			digitalWrite(this->layerPins[layerIndex], LOW);
			break;
		
		case DEDICATED_SHIFT_REGISTER:
			// TODO: Implement this
			digitalWrite(this->layerPins[layerIndex], LOW);
			break;
	}
}

#endif
