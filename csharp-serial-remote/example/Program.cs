﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CubeDuino.Example
{
	class Program
	{
		static void Main(string[] args)
		{
			CubeDuino cube = new CubeDuino(4, "COM4", 57600);
			cube.Clear();

			while (true)
			{
				IncrementPattern(cube);
				TrianglePattern(cube);
				RandomPattern(cube);
				LayerPattern(cube);
			}
		}

		private static void IncrementPattern(CubeDuino cube)
		{
			for (int i = 0; i < 64; i++)
			{
				cube.Set(i);
				delay(40);
			}

			for (int i = 0; i < 64; i++)
			{
				cube.Set(i, false);
				delay(40);
			}
		}

		private static void TrianglePattern(CubeDuino cube)
		{
			for (int frame = 0; frame < 10; frame++)
			{
				for (int z = 0; z <= frame; z++)
				{
					int a = frame - z;
					for (int b = 0; b <= a; b++)
					{
						cube.Set(b, a - b, z);
						cube.Set(a - b, b, z);
					}
				}
				delay(100);
			}

			for (int frame = 0; frame < 10; frame++)
			{
				for (int z = 0; z <= frame; z++)
				{
					int a = frame - z;
					for (int b = 0; b <= a; b++)
					{
						cube.Set(b, a - b, z, false);
						cube.Set(a - b, b, z, false);
					}
				}
				delay(100);
			}
		}

		private static void RandomPattern(CubeDuino cube)
		{
			Random random = new Random();
			for (int i = 0; i < 500; i++)
			{
				cube.Set(random.Next(64), random.Next(2) == 1);
				delay(5);
			}
			cube.Clear();
		}

		private static void LayerPattern(CubeDuino cube)
		{
			byte[] zeroBuffer = new byte[cube.BlocksPerLayer];
			byte[] fillBuffer = new byte[cube.BlocksPerLayer];
			for (int i = 0; i < cube.BlocksPerLayer; i++)
				fillBuffer[i] = 0xFF;

			for (int repeat = 0; repeat < 5; repeat++)
			{
				for (int i = 0; i < 4; i++)
				{
					cube.Set(i - 1, zeroBuffer);
					cube.Set(i, fillBuffer);
					delay((int)Math.Pow(i - 2, 2) * 2 + 100);
				}

				for (int i = 1; i < 4; i++)
				{
					cube.Set(4 - i + 1, zeroBuffer);
					cube.Set(4 - i, fillBuffer);
					delay((int)Math.Pow(i - 2, 2) * 2 + 100);
				}
				cube.Clear();
			}
		}

		private static void delay(int ms)
		{
			Thread.Sleep(ms);
		}
	}
}
