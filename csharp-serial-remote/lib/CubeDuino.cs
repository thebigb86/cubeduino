﻿using System;
using System.Threading;
using CubeDuino.Commands;

namespace CubeDuino
{
	public class CubeDuino
	{
		protected SerialController controller;

		public int Size { get; private set; }
		public int Dimension { get; private set; }
		public int LayerSize { get; private set; }
		public int BlocksPerLayer { get; private set; }

		public CubeDuino(int dimension, string serialPortName, int baudRate = 57600)
		{
			this.Size = (int)Math.Pow(dimension, 3);
			this.Dimension = dimension;
			this.LayerSize = (int)Math.Pow(dimension, 2);
			this.BlocksPerLayer = (int)Math.Ceiling(this.LayerSize / 8.0);

			this.controller = new SerialController(serialPortName, baudRate);
			Thread.Sleep(100);
			this.controller.Feed(new Sync());
			Thread.Sleep(100);
		}

		public CubeDuino Set(int x, int y, int z, bool value = true)
		{
			//if (x < 0 || x > this.Dimension || y < 0 || y > this.Dimension || z < 0 || z > this.Dimension)
			//	return this;

			this.controller.Feed(new SetByCoordinates((byte)x, (byte)y, (byte)z, value));
			return this;
		}

		public CubeDuino Set(int index, bool value = true)
		{
			//if (index < 0 || index > this.Size)
			//	return this;

			this.controller.Feed(new SetByIndex((ushort)index, value));
			return this;
		}

		public CubeDuino Set(int layerIndex, byte[] bitBlocks)
		{
			//if (layerIndex < 0 || layerIndex > this.Dimension)
			//	return this;

			this.controller.Feed(new SetByLayer((byte)layerIndex, bitBlocks));
			return this;
		}

		public CubeDuino Set(int layerIndex, bool[] bits)
		{
			//if (layerIndex < 0 || layerIndex > this.Dimension)
			//	return this;

			byte[] bitBlocks = new byte[this.BlocksPerLayer];

			int blockCounter = 0;
			for (int i = 0; i < bits.Length; i++)
			{
				if (bits[i])
					bitBlocks[blockCounter] |= (byte)(1 << i);

				if (i % 8 == 0) blockCounter++;
			}

			this.controller.Feed(new SetByLayer((byte)layerIndex, bitBlocks));
			return this;
		}

		public CubeDuino Clear()
		{
			byte[] zeroBuffer = new byte[this.BlocksPerLayer];
			for (int i = 0; i < Dimension; i++)
			{
				this.Set(i, zeroBuffer);
			}
			return this;
		}

		~CubeDuino()
		{
			this.controller.Dispose();
		}
	}
}
