﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Collections.Concurrent;

namespace CubeDuino
{
	public class SerialController : IDisposable
	{
		protected ConcurrentQueue<SerialCommand> commandBuffer = new ConcurrentQueue<SerialCommand>();

		protected SerialPort port;

		private bool disposed;

		public SerialController(string portName, int baudRate)
		{
			this.port = new SerialPort(portName, baudRate);
			this.port.Open();
			this.port.DataReceived += port_DataReceived;

			new Thread(DataFeeder) { IsBackground = true }.Start();
		}

		void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			Console.Write(this.port.ReadExisting());
		}

		public void Feed(SerialCommand command)
		{
			this.commandBuffer.Enqueue(command);
		}

		protected void DataFeeder()
		{
			while (true)
			{
				SerialCommand item;
				if (this.commandBuffer.TryDequeue(out item))
				{
					byte[] buffer = item.EncodeCommand();
					this.port.Write(buffer, 0, buffer.Length);
				}
				Console.WriteLine(String.Format("Command buffer size: {0}", this.commandBuffer.Count));
			}
		}

		public bool Close()
		{
			try
			{
				this.port.DiscardInBuffer();
				this.port.Close();
				this.port.Dispose();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public void Dispose()
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;

			this.Close();
		}

		~SerialController()
		{
			this.Dispose();
		}
	}
}
