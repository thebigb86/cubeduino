﻿using System;

namespace CubeDuino.Commands
{
	public class SetByLayer : SerialCommand
	{
		public const int COMMAND_INDEX = 3;

		public byte LayerIndex { get; set; }

		public byte[] BitBlocks { get; set; }

		public SetByLayer(byte layerIndex, byte[] bitBlocks)
		{
			this.LayerIndex = layerIndex;
			this.BitBlocks = bitBlocks;
		}

		public override byte[] EncodeCommand()
		{
			byte[] buffer = new byte[3 + this.BitBlocks.Length];

			buffer[0] = SerialCommand.COMMAND_FLAG | COMMAND_INDEX;
			buffer[1] = this.LayerIndex;
			buffer[2] = (byte)this.BitBlocks.Length;
			Array.Copy(this.BitBlocks, 0, buffer, 3, this.BitBlocks.Length);

			return buffer;
		}
	}
}
