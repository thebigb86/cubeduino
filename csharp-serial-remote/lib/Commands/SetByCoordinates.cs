﻿using System;

namespace CubeDuino.Commands
{
	public class SetByCoordinates : SerialCommand
	{
		public const int COMMAND_INDEX = 1;

		public byte X { get; set; }
		public byte Y { get; set; }
		public byte Z { get; set; }
		public bool Enable { get; set; }

		public SetByCoordinates(byte x, byte y, byte z, bool enable = true)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
			this.Enable = enable;
		}

		public override byte[] EncodeCommand()
		{
			byte[] buffer = new byte[4];

			buffer[0] = SerialCommand.COMMAND_FLAG | COMMAND_INDEX;

			if (this.Enable)
				buffer[0] |= SerialCommand.ENABLE_FLAG;

			buffer[1] = this.X;
			buffer[2] = this.Y;
			buffer[3] = this.Z;

			return buffer;
		}
	}
}
