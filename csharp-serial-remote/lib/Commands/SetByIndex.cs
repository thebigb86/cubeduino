﻿using System;

namespace CubeDuino.Commands
{
	public class SetByIndex : SerialCommand
	{
		public const int COMMAND_INDEX = 2;

		public ushort Index { get; set; }
		public bool Enable { get; set; }

		public SetByIndex(ushort index, bool enable = true)
		{
			this.Index = index;
			this.Enable = enable;
		}

		public override byte[] EncodeCommand()
		{
			byte[] buffer = new byte[3];

			buffer[0] = SerialCommand.COMMAND_FLAG | COMMAND_INDEX;

			if (this.Enable)
				buffer[0] |= SerialCommand.ENABLE_FLAG;

			buffer[1] = (byte)(this.Index >> 8);
			buffer[2] = (byte)(this.Index);

			return buffer;
		}
	}
}
