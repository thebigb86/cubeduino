﻿using System;

namespace CubeDuino.Commands
{
	public class Sync : SerialCommand
	{
		public override byte[] EncodeCommand()
		{
			return new byte[] { 0x55, 0xAA };
		}
	}
}
