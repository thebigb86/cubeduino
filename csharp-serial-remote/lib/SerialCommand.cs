﻿using System;

namespace CubeDuino
{
	public abstract class SerialCommand
	{
		public const int COMMAND_FLAG = 0x80;    // B(1000 0000)
		public const int ENABLE_FLAG = 0x40;     // B(0100 0000)

		public abstract byte[] EncodeCommand();
	}
}
