#include <LedCube.h>

void setup() 
{
	// Initialize the LedCube object
	// Dimension          = 4
	// Refresh interval   = 4000 microseconds - 1ms (1000 microseconds per layer - 1ms)
	// Data pin           = 7
	// Latch pin          = 6
	// Clock pin          = 5
	// Layer pin-out type = LAYER_DEDICATED_PINS
	// Layer pins         = pin 8 to 11
	Cube.begin(4, 4000, 7, 6, 5, LAYER_DEDICATED_PINS, 8, 9, 10, 11);
	Cube.setBitOrder(LSBFIRST);
}

void loop() {
	incrementPattern();
	trianglePattern();
	randomPattern();
	layerPattern();
}

void incrementPattern()
{
	for(byte i = 0; i < 64; i++)
	{
		Cube.write(i);
		delay(40);
	}
  
	for(byte i = 0; i < 64; i++)
	{
		Cube.write(i, LOW);
		delay(40);
	}
}

void trianglePattern()
{
	for(byte frame = 0; frame < 10; frame++)
	{
		for(byte z = 0; z <= frame; z++)
		{
			byte a = frame - z;
			for(byte b = 0; b <= a; b++)
			{
				Cube.write(b, a - b, z);
				Cube.write(a - b, b, z);
			}
		}
		delay(100);
	}
  
	for(byte frame = 0; frame < 10; frame++)
	{
		for(byte z = 0; z <= frame; z++)
		{
			byte a = frame - z;
			for(byte b = 0; b <= a; b++)
			{
				Cube.write(b, a - b, z, LOW);
				Cube.write(a - b, b, z, LOW);
			}
		}
		delay(100);
	}
}

void randomPattern()
{
	randomSeed(analogRead(A0));
	for (unsigned int i = 0; i < 500; i++)
	{
		Cube.write(random(64), random(2));
		delay(5);
	}
	Cube.clear();
}

void layerPattern()
{
	for (byte repeat = 0; repeat < 5; repeat++)
	{
		for (byte i = 0; i < 4; i++)
		{
			Cube.writeLayerValue(i - 1, 0x0000);
			Cube.writeLayerValue(i, 0xFFFF);
			delay(pow(i - 2, 2) * 2 + 100);
		}
	
		for (byte i = 1; i < 4; i++)
		{
			Cube.writeLayerValue(4 - i + 1, 0x0000);
			Cube.writeLayerValue(4 - i, 0xFFFF);
			delay(pow(i - 2, 2) * 2 + 100);
		}
		Cube.clear();
	}
}

