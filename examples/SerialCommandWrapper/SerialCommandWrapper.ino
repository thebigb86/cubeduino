#include <LedCube.h>

/*! \def COMMAND        Bit flag indicating that the byte might be a command */
/*! \def ENABLE         Bit flag indicating that the command is issuing the enabling of an entity */

/*! \def SET_BY_COORD   A value indicating that an LED state should be changed at the x, y, and z position that will follow */
/*! \def SET_BY_INDEX   A value indicating that an LED state should be changed at zero-based index that will follow */
/*! \def SET_BY_LAYER   A value indicating that the state for an entire LED layer should be changed, at the index that will follow, with the value that will follow after */

/*! \def SYNC           A value indicating that the remote device might want to initiate synchronization */
/*! \def CONFIRM_SYNC   A value reinforcing the probability that the remote device is invoking synchronization */

#define COMMAND     0b10000000
#define ENABLE      0b01000000

#define SET_BY_COORD 1
#define SET_BY_INDEX 2
#define SET_BY_LAYER 3

#define SYNC         0b01010101
#define CONFIRM_SYNC 0b10101010
/*
	SOMEWHAT PROPER SYNC

/*! \def BEGIN_SYNC         A value indicating that the remote device might want to initiate synchronization * /
/*! \def SYNC_STEP_A        A value that is expected for step A in the synchronization sequence * /
/*! \def SYNC_STEP_B        A value that is expected for step B in the synchronization sequence * /
/*! \def SYNC_STEP_LENGTH   The number of bytes over which a step is repeated in the synchronization sequence * /
/*! \def SYNC_SEQ_LENGTH    The total sequence length (excluding the BEGIN_SYNC indicator) * /	
#define BEGIN_SYNC          0b01010101
#define SYNC_STEP_A         0b00000000
#define SYNC_STEP_B         0b11111111
#define SYNC_STEP_LENGTH    ceil((Cube.getBlocksPerLayer() + 3) / 2.0) // Since the SET_BY_LAYER command is the longest, we want the synchronization sequence to be as long as that
#define SYNC_SEQ_LENGTH     SYNC_STEP_LENGTH * 2
*/

#define READ_BYTE_TIMEOUT 50

#define hasFlags(byte, flag) ((byte & flag) == flag)
#define hasLowerNibble(byte, nibble) ((byte & 0xF) == nibble)

bool isSynchronized;
byte* setByLayerBuffer;

void setup()
{
	// Initialize the LedCube object
	// Dimension          = 4
	// Refresh interval   = 4000 microseconds - 1ms (1000 microseconds per layer - 1ms)
	// Data pin           = 7
	// Latch pin          = 6
	// Clock pin          = 5
	// Layer pin-out type = LAYER_DEDICATED_PINS
	// Layer pins         = pin 8 to 11
	Cube.begin(4, 4000, 7, 6, 5, LAYER_DEDICATED_PINS, 8, 9, 10, 11);
	Cube.setBitOrder(LSBFIRST);

	setByLayerBuffer = new byte[Cube.getBlocksPerLayer()];
	
	Serial.begin(57600);
	while(!Serial);
}

void loop()
{
	byte input = readByteBlocking();
	
	if(input == SYNC)
	{
		synchronize();
		return;
	}
	
	if(!isSynchronized || !hasFlags(input, COMMAND))
		return;
		
	if(hasLowerNibble(input, SET_BY_COORD))
	{
		byte x = readByteBlocking();
		byte y = readByteBlocking();
		byte z = readByteBlocking();
		bool state = hasFlags(input, ENABLE);
		Cube.write(x, y, z, state);
		return;
	}
	
	if(hasLowerNibble(input, SET_BY_INDEX))
	{
		unsigned int index = readIntBlocking();
		bool state = hasFlags(input, ENABLE);
		Cube.write(index, state);
		return;
	}
	
	if(hasLowerNibble(input, SET_BY_LAYER))
	{
		byte index = readByteBlocking();
		byte expectedLength = Cube.getBlocksPerLayer();
		byte receivingLength = readByteBlocking();
				
		for(int i = 0; i < receivingLength; i++)
		{
			if(i < expectedLength)
				Cube.writeBlock(index, i, readByteBlocking());
			else
				readByteBlocking();
		}
		
		return;
	}
}

/**
 * Verifies whether the data sequence that is being received is a synchronization sequence, and if so, updates the isSynchronized state variable
 */
void synchronize()
{
	while(Serial.available() == 0);
	
	if(Serial.peek() == CONFIRM_SYNC)
	{
		Serial.read();
		isSynchronized = true;
		Serial.println("Synchronized");
	}
	
	/* 
		SOMEWHAT PROPER SYNC
	
	while(Serial.available() < SYNC_SEQ_LENGTH);
	
	bool syncFailed = false;
	for(byte i = 0; i < SYNC_SEQ_LENGTH; i++)
	{
		if((i < SYNC_STEP_LENGTH && serialPeek(i) != SYNC_STEP_A) || (i >= SYNC_STEP_LENGTH && serialPeek(i) != SYNC_STEP_B))
		{
			syncFailed = true;
			break;
		}
	}
	
	if(!syncFailed)
	{
		for(byte i = 0; i < SYNC_SEQ_LENGTH; i++)
			Serial.read();
		
		isSynchronized = true;
	}
	*/
}

/**
 * Reads a single byte from the serial port (blocking)
 */
inline byte readByteBlocking()
{
	unsigned int timeout = millis() + READ_BYTE_TIMEOUT;
	while(Serial.available() == 0 && timeout > millis());
	return Serial.read();
}

/**
 * Reads two bytes from the serial port (blocking) and returns them as a single unsigned int (16 bits)
 */
inline unsigned int readIntBlocking()
{
	byte a = readByteBlocking();
	byte b = readByteBlocking();
	return (a << 8) | b;
}

/*
	NEED THIS FOR PROPER SYNC

/**
 * Takes a peek at the serial receive buffer at the provided index
 * @param index the index of the byte to peek at in the serial receive buffer
 * /
int serialPeek(int index)
{
	;
}
*/
