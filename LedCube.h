#ifndef LEDCUBE_h
#define LEDCUBE_h

#include <Arduino.h>
#include <LedData.hpp>

/*! \def LAYER_DEDICATED_PINS       Indicates that each layer has a dedicated connection to the Arduino */
/*! \def DEDICATED_SHIFT_REGISTER   The layers are connected to the Arduino through a shift register */

#define LAYER_DEDICATED_PINS     0
#define DEDICATED_SHIFT_REGISTER 1

/*! \def LAYER_PIN_DATA	    Array index alias: Array item contains the data pin index for the dedicated shift register for the layers */
/*! \def LAYER_PIN_LATCH    Array index alias: Array item contains the latch pin index for the dedicated shift register for the layers */
/*! \def LAYER_PIN_CLOCK    Array index alias: Array item contains the clock pin index for the dedicated shift register for the layers */

#define LAYER_PIN_DATA  0
#define LAYER_PIN_LATCH 1
#define LAYER_PIN_CLOCK 2

class LedCube
{
	public:
		/**
		 * Initialize the LedCube object with the relevant pin-outs
		 * @param dimension the size of the cube along one of the axis
		 * @param refreshInterval the time to wait before rendering the cube again (the layer render timing is spread equally over this interval)
		 * @param pinData the index of the data pin that connects to the shift registers
		 * @param pinLatch the index of the latch pin that connects to the shift registers
		 * @param pinClock the index of the clock pin that connects to the shift registers
		 * @param layerPinOutType the type of pin-out that connects to the layer transistors (or MOSFETS); either LAYER_DEDICATED_PINS, DEDICATED_SHIFT_REGISTER or SHARED_SHIFT_REGISTER
		 * @param ... in the case of LAYER_DEDICATED_PINS, the individual pin indexes are expected as variadic parameters; in the case of DEDICATED_SHIFT_REGISTER, the data, latch and clock pin indexes are expected as variadic parameters
		 */
		void begin(byte dimension, unsigned long refreshInterval, byte pinData, byte pinLatch, byte pinClock, byte layerPinOutType, ...);
		
		/**
		 * Clears the cube
		 */
		void clear();
		
		/**
		 * Clears the layer of the provided index
		 * @param layerIndex the index of the layer to clear
		 */
		void clearLayer(byte layerIndex);
		
		/**
		 * Writes the provided state to the LED at the provided index
		 * @param index the index of the LED to write to
		 * @param state the state to write to the LED (defaults to HIGH)
		 */
		void write(unsigned int index, bool state = HIGH);
		
		/**
		 * Writes the provided state to the LED at the provided coordinates
		 * @param x the position along the x axis of the LED to write to
		 * @param y the position along the y axis of the LED to write to
		 * @param z the position along the z axis of the LED to write to
		 * @param state the state to write to the LED (defaults to HIGH)
		 */
		void write(byte x, byte y, byte z, bool state = HIGH);
		
		/**
		 * Writes the provided value to the layer and block of the provided index
		 * @param layerIndex the index of the layer to write to
		 * @param blockIndex the index of the block to write to
		 * @param value the value to write to the layer
		 */
		void writeBlock(byte layerIndex, byte blockIndex, byte value);
		
		/**
		 * Writes the provided value to the layer of the provided index
		 * @param layerIndex the index of the layer to write to
		 * @param value the value to write to the layer
		 */
		void writeLayerValue(byte layerIndex, unsigned long value);

		/**
		 * Set a custom bit order for shifting out (MSBFIRST is the default configuration)
		 * @param order the bit order to set (either LSBFIRST or MSBFIRST) 
		 */
		void setBitOrder(byte order);
		
		/**
		 * Gets the total number of configured LEDs
		 */
		unsigned int getSize();
		
		/**
		 * Gets the size of the cube along one of the axis
		 */
		byte getDimension();
		
		/**
		 * Gets the number of LEDs in one layer
		 */
		byte getLayerSize();
		
		/**
		 * Gets the number of 8-bit blocks in a layer
		 */
		byte getBlocksPerLayer();
		
		/**
		 * Internal timer callback that invokes the rendering and chained routine
		 */	
		void timerCallback();
		
		/**
		 * Sets the provided routine to be called (in chain) at the configured refresh interval
		 * @param chainedRoutine routine to be called
		 */
		void setChainedRoutine(void (*chainedRoutine)());
		
	private:
		/**
		 * Static wrapper for the timer callback
		 */	
		static void staticTimerCallback();

		/**
		 * Renders the next layer (circularly)
		 */
		void renderLayer();
		
		/**
		 * Enable the layer with the provided index
		 */
		void enableLayer(byte layerIndex);
		
		/**
		 * Disable the layer with the provided index
		 */
		void disableLayer(byte layerIndex);
		
		/**
		 * Holds an instance to the cube data storage class
		 */
		LedData ledData;
		
		/**
		 * Holds the index of the layer that is currently being rendered
		 */
		byte currentLayer;
		
		/**
		 * Holds the order in which to shift out bits
		 */
		byte bitOrder;

		/**
		 * Holds the index of the data pin that connects to the shift registers
		 */
		byte pinData;
		
		/**
		 * Holds the index of the latch pin that connects to the shift registers
		 */
		byte pinLatch;
		
		/**
		 * Holds the index of the clock pin that connects to the shift registers
		 */
		byte pinClock;
		
		/**
		 * Holds the type of pin-out that connects to the layer transistors (or MOSFETS); either LAYER_DEDICATED_PINS, DEDICATED_SHIFT_REGISTER
		 */
		byte layerPinOutType;
		
		/**
		 * Holds an array of the pins that connect to the layers (or the pins that connect to a dedicated shift register for the layers)
		 */
		byte* layerPins;
		
		/**
		 * Holds the refresh interval
		 */
		unsigned long refreshInterval;
		
		/**
		 * Holds a routine that is to be called (in chain) at the configured refresh interval
		 */
		void (*chainedRoutine)();
};

extern LedCube Cube;

#endif
