#ifndef LEDDATA_h
#define LEDDATA_h

#include <Arduino.h>

class LedData
{
	public:
		/**
		 * Initialize the LedCube data representation
		 */
		void begin(byte dimension);
		
		/**
		 * Clears the cube
		 */
		void clear();
		
		/**
		 * Clears the layer of the provided index
		 * @param layerIndex the index of the layer to clear
		 */
		void clearLayer(byte layerIndex);	
		
		/**
		 * Sets the provided bit state for the provided index
		 * @param indexInCube the index of the bit to set the value of
		 * @param state the state to set the bit to
		 */	
		void setBit(unsigned int indexInCube, bool state = HIGH);
		
		/**
		 * Gets the bit state for the provided index
		 * @param indexInCube the index of the bit to get the value of
		 */
		bool getBit(unsigned int indexInCube);
	
		/**
		 * Sets the provided data for the provided layer index
		 * @param layerIndex the index of the layer to set the data for
		 * @param data the data to set (divided in 8-bit blocks)
		 */
		void setLayer(byte layerIndex, byte* data);
		
		/**
		 * Sets the provided value for the provided layer index and block index
		 * @param layerIndex the index of the layer to set the data for
		 * @param blockIndex the index of the block to set the data for
		 * @param value the data to set (divided in 8-bit blocks)
		 */
		void setBlock(byte layerIndex, byte blockIndex, byte value);
		
		/**
		 * Gets the data for the provided layer index, divided in 8-bit blocks
		 * @param layerIndex the index of the layer to get the data for
		 */
		byte* getLayer(byte layerIndex);
		
		/**
		 * Gets the total number of configured LEDs
		 */
		unsigned int getSize();
		
		/**
		 * Gets the size of the cube along one of the axis
		 */
		byte getDimension();
		
		/**
		 * Gets the number of LEDs in one layer
		 */
		byte getLayerSize();
		
		/**
		 * Gets the number of 8-bit blocks in a layer
		 */
		byte getBlocksPerLayer();
		
	private:
		/**
		 * Holds virtual representation of all LEDs in the cube, sequentially as bits, divided in 8-bit blocks for each layer
		 */
		byte** data;
		
		/**
		 * The total number of configured LEDs
		 */
		unsigned int size;
		
		/**
		 * Holds the size of the cube along one of the axis
		 */
		byte dimension;
		
		/**
		 * Holds the number of LEDs in one layer
		 */
		unsigned int layerSize;
		
		/**
		 * Holds the number of 8-bit blocks in a layer
		 */
		byte blocksPerLayer;
};

#endif
