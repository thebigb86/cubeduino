#ifndef LEDDATA_cpp
#define LEDDATA_cpp

#include <LedData.hpp>

void LedData::begin(byte dimension)
{
	this->dimension = dimension;
	this->size = dimension * dimension * dimension;
	this->layerSize = dimension * dimension;
	this->blocksPerLayer = ceil(this->layerSize / 8.0);
	
	this->data = new byte*[dimension];
	for(byte i = 0; i < dimension; i++)
	{
		this->data[i] = new byte[this->blocksPerLayer];
	}
	this->clear();
}

void LedData::clear()
{
	for(byte layer = 0; layer < this->dimension; layer++)
	{
		for(byte block = 0; block < this->blocksPerLayer; block++)
		{	
			this->data[layer][block] = 0;
		}
	}
}

void LedData::clearLayer(byte layerIndex)
{
	if(layerIndex >= this->dimension)
		return;

	for(byte block = 0; block < this->blocksPerLayer; block++)
	{	
		this->data[layerIndex][block] = 0;
	}
}

void LedData::setBit(unsigned int indexInCube, bool state)
{	
	if(indexInCube >= this->size)
		return;
	
	byte indexInLayer = indexInCube % this->layerSize;
	byte indexInBlock = indexInLayer % 8;
	
	byte layerIndex = floor(indexInCube / (float)this->layerSize);
	byte blockIndex = floor(indexInLayer / 8);
	
	bitWrite(this->data[layerIndex][blockIndex], indexInBlock, state);
}

bool LedData::getBit(unsigned int indexInCube)
{
	if(indexInCube >= this->size)
		return false;
	
	byte indexInLayer = indexInCube % this->layerSize;
	byte indexInBlock = indexInLayer % 8;
	
	byte layerIndex = floor(indexInCube / (float)this->layerSize);
	byte blockIndex = floor(indexInLayer / 8);
	
	return bitRead(this->data[layerIndex][blockIndex], indexInBlock);
}

void LedData::setLayer(byte layerIndex, byte* data)
{
	if(layerIndex >= this->dimension)
		return;
	
	for(byte i = 0; i < this->blocksPerLayer; i++)
	{
		this->data[layerIndex][i] = data[i];
	}
}

void LedData::setBlock(byte layerIndex, byte blockIndex, byte value)
{
	if(layerIndex >= this->dimension || blockIndex >= this->blocksPerLayer)
		return;
	
	this->data[layerIndex][blockIndex] = value;
}

byte* LedData::getLayer(byte layerIndex)
{
	if(layerIndex >= this->dimension)
		return NULL;
	
	return this->data[layerIndex];
}

unsigned int LedData::getSize()
{
	return this->size;
}

byte LedData::getDimension()
{
	return this->dimension;
}

byte LedData::getLayerSize()
{
	return this->layerSize;
}

byte LedData::getBlocksPerLayer()
{
	return this->blocksPerLayer;
}

#endif
